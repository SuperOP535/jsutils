String.prototype.byteLengthAt = function(index) {
	var codePoint = this.codePointAt(index);
	if(codePoint <= 255) return 1;
	if(codePoint <= 65535) return 2;
	if(codePoint <= 16777216) return 3;
	if(codePoint <= 4294967296) return 4;
	return -1;
};