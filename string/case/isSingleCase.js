String.prototype.isSingleCase = function() {
	return this.toLowerCase() === this.toUpperCase();
};