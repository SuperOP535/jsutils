String.prototype.toCodePointArray = function() {
	var array = [];
	for(var i = 0; i < this.length; i++)
		array.push(this.codePointAt(i));
	return array;
};