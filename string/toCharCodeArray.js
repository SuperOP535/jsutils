String.prototype.toCharCodeArray = function() {
	var array = [];
	for(var i = 0; i < this.length; i++)
		array.push(this.charCodeAt(i));
	return array;
};