String.prototype.trimIndent = function() {
	return this.replace(/^\s+/gm, '');
};