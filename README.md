# jsutils

Small utilities for JavaScript projects

### Code style
Tabs, LF, no newline at end of file, space after function constructor, var, and semicolons.

### Tips
Press T to quickly search for functions.