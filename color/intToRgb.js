function intToRgb(rgb) {
	return [(rgb >> 16) & 255, (rgb >> 8) & 255, rgb & 255];
}