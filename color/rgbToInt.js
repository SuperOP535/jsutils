function rgbToInt(rgb) {
	return 256 * 256 * rgb[0] + 256 * rgb[1] + rgb[2];
}